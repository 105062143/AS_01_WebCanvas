if (canvas.getContext) { //判斷是否支援
  var ctx = canvas.getContext('2d');
}else {
  alert('your browser not support canvas')
  //如果不支援
};

window.onload = function () {
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');

  document.getElementById('pencil').addEventListener('click', pencil, false);
  document.getElementById('eraser').addEventListener('click', eraser, false);
  document.getElementById('line').addEventListener('click', line, false);
  document.getElementById('text').addEventListener('click', text, false);
  document.getElementById('circle').addEventListener('click', circle, false);
  document.getElementById('rectangle').addEventListener('click', rectangle, false);
  document.getElementById('triangle').addEventListener('click', triangle, false);
  document.getElementById('fill').addEventListener('click', fill, false);
  document.getElementById('stroke').addEventListener('click', stroke, false);
  document.getElementById('undo').addEventListener('click', undo, false);
  document.getElementById('redo').addEventListener('click', redo, false);
  document.getElementById('reset').addEventListener('click', reset, false);
  document.getElementById('download').addEventListener('click', download, false);

  document.getElementById('size').addEventListener('change', changeSize, false);
  document.getElementById('red').addEventListener('change', changeColor, false);
  document.getElementById('green').addEventListener('change', changeColor, false);
  document.getElementById('blue').addEventListener('change', changeColor, false);
  document.getElementById('fontsize').addEventListener('change', changefontSize, false);
  document.getElementById('fontstyle').addEventListener('change', changefontStyle, false);
  canvas.addEventListener('mousedown', mouseDown, false);
  canvas.addEventListener('mousemove', mouseMove, false);
  canvas.addEventListener('mouseup', mouseUp, false);
}
var x = 0;
var y = 0;
var dot = 0;
var modes = ['pencil', 'eraser', 'line', 'text', 'circle', 'rectangle', 'triangle', 'undo'];
var mode = modes[0];
var drawing = false;
var num = 1;
PicArray = new Array();
Step = -1;
canvas.style.cursor = "url('cursor_pencil.png'), auto";
var filled = 1;

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

function mouseDown(evt) {
  switch (mode) {
    case 'pencil': 
      drawing = true;
      var mousePos = getMousePos(canvas, evt);
      ctx.beginPath();
      ctx.moveTo(mousePos.x, mousePos.y);
      evt.preventDefault();
      //canvas.addEventListener('mousemove', mouseMove, false);
      break;
    case 'eraser': 
      drawing = true;
      var mousePos = getMousePos(canvas, evt);
      ctx.strokeStyle = 'white';
      ctx.beginPath();
      ctx.moveTo(mousePos.x, mousePos.y);
      evt.preventDefault();
      //canvas.addEventListener('mousemove', mouseMove, false);
      break;
    case 'line': 
      drawing = true;
      var mousePos = getMousePos(canvas, evt);
      ctx.beginPath();
      ctx.moveTo(mousePos.x, mousePos.y);
      evt.preventDefault();
      //canvas.addEventListener('mousemove', mouseMove, false);
      break;
    case 'text': 
      var mousePos = getMousePos(canvas, evt);
      //ctx.lineWidth = 10;
      if (filled) ctx.fillText(document.getElementById('Text').value, mousePos.x, mousePos.y);
      else ctx.strokeText(document.getElementById('Text').value, mousePos.x, mousePos.y);
      queue();
      break;
    case 'circle': 
      drawing = true;
      var mousePos = getMousePos(canvas, evt);
      ctx.beginPath();
      x = mousePos.x;
      y = mousePos.y;
      break;
    case 'rectangle': 
      drawing = true;
      var mousePos = getMousePos(canvas, evt);
      ctx.beginPath();
      x = mousePos.x;
      y = mousePos.y;
      break;
    case 'triangle': 
      drawing = true;
      break;
  }
}

function mouseMove(evt) {
  switch (mode) {
    case 'pencil': 
      if (drawing) {
        var mousePos = getMousePos(canvas, evt);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
      }
      break;
    case 'eraser': 
      if (drawing) {
        var mousePos = getMousePos(canvas, evt);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
      }
      break;
    case 'line': 
      break;
    case 'text': 
      break;
    case 'circle': 
      break;
    case 'rectangle': 
      break;
    case 'triangle': 
      break;
  }
}

function mouseUp(evt) {
  switch (mode) {
    case 'pencil': 
      drawing = false;
      queue();
      //canvas.removeEventListener('mousemove', mouseMove, false);
      break;
    case 'eraser': 
      drawing = false;
      queue();
      //canvas.removeEventListener('mousemove', mouseMove, false);
      break;
    case 'line': 
      var mousePos = getMousePos(canvas, evt);
      ctx.lineTo(mousePos.x, mousePos.y);
      ctx.stroke();
      drawing = false;
      queue();
      //canvas.removeEventListener('mousemove', mouseMove, false);
      break;
    case 'text': 
      break;
    case 'circle': 
      var mousePos = getMousePos(canvas, evt);
      var rx = (mousePos.x-x)/2;
      var ry = (mousePos.y-y)/2;
      var r = Math.sqrt(rx*rx+ry*ry);
      ctx.arc(rx+x,ry+y,r,0,Math.PI*2, false);
      if (filled) ctx.fill();
      else ctx.stroke();
      drawing = false;
      queue();
      break;
    case 'rectangle': 
      var mousePos = getMousePos(canvas, evt);
      if (filled) ctx.fillRect(x, y, mousePos.x-x, mousePos.y-y);
      else ctx.strokeRect(x, y, mousePos.x-x, mousePos.y-y);
      drawing = false;
      queue();
      break;
    case 'triangle': 
      var mousePos = getMousePos(canvas, evt);
      if (drawing) {
        if (dot == 0) {
          ctx.beginPath();
          ctx.moveTo(mousePos.x, mousePos.y);
          evt.preventDefault();
          drawing = false;
          dot++;
        } else if (dot == 1) {
          ctx.lineTo(mousePos.x, mousePos.y);
          drawing = false;
          dot++;
        } else if (dot >= 2) {
          ctx.lineTo(mousePos.x, mousePos.y);
          ctx.closePath();
          if (filled) ctx.fill();
          else ctx.stroke();
          drawing = false;
          dot = 0;
          queue();
        }
      }
      break;
  }
}

function pencil() {  //pencil
  mode = modes[0];
  changeColor();
  //ctx.strokeStyle = 'black';
  canvas.style.cursor = "url('Cursors/cursor_pencil.png'), auto";
}

function eraser() {  //eraser
  mode = modes[1];
  canvas.style.cursor = "url('Cursors/cursor_eraser.png'), auto";
}

function line() {  //line
  mode = modes[2];
  changeColor();
  //ctx.strokeStyle = 'black';
  canvas.style.cursor = "url('Cursors/cursor_line.png'), auto";
}

function text() {  //text
  mode = modes[3];
  //ctx.strokeStyle = 'black';
  canvas.style.cursor = "url('Cursors/cursor_text.png'), auto";
}

function circle() {  //circle
  mode = modes[4];
  changeColor();
  //ctx.strokeStyle = 'black';
  canvas.style.cursor = "url('Cursors/cursor_circle.png'), auto";
}

function rectangle() {  //rectangle
  mode = modes[5];
  changeColor();
  //ctx.strokeStyle = 'black';
  canvas.style.cursor = "url('Cursors/cursor_rectangle.png'), auto";
}

function triangle() {  //triangle
  mode = modes[6];
  dot = 0;
  changeColor();
  //ctx.strokeStyle = 'black';
  canvas.style.cursor = "url('Cursors/cursor_triangle.png'), auto";
}

function undo() {  //undo
  canvas.style.cursor = "auto";
}

function changeSize(){
  ctx.lineWidth = this.value;
  //將數值寫入到 strokeStyle內即可
};

function changeColor(){
  var r = document.getElementById('red').value;
  var g = document.getElementById('green').value;
  var b = document.getElementById('blue').value;
  ctx.fillStyle = "rgb("+r+","+g+","+b+")";
  ctx.strokeStyle = "rgb("+r+","+g+","+b+")";
};

function changefontSize(){
  var font_x = "" + String(this.value) +"px ";
  font_x += String(document.getElementById('fontstyle').value);
  //font_x += "Arial";
  ctx.font = font_x;
};
function changefontStyle(){
  var font_x = "" + String(document.getElementById('fontsize').value)+"px ";
  font_x += String(this.value);
  ctx.font = font_x;
};

function fill() {
  filled = 1;
};
function stroke() {
  filled = 0;
}

function reset(clear) {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  //ctx.fillStyle = "white";
  //ctx.fillRect(0,0,canvas.width,canvas.height);
  if (clear != 1) queue();
};

function download() {
  document.getElementById('Download').href = canvas.toDataURL("image/jpg");
  document.getElementById('Download').download = "my_canvas_" + num.toString();
  num++;
};

function undo() {
  if (Step > 0) {
      Step--;
      var Pic = new Image();
      Pic.src = PicArray[Step];
      reset(1);
      Pic.onload = function(){ ctx.drawImage(Pic, 0, 0); }
  }
  else {
    if (Step == 0) Step--;
    reset(1);
  }
}
function redo() {
  if (Step < PicArray.length-1) {
    Step++;
    var Pic = new Image();
    Pic.src = PicArray[Step];
    Pic.onload = function(){ ctx.drawImage(Pic, 0, 0); };
  }
}
function queue() {
  Step++;
  PicArray[Step] = canvas.toDataURL();
}