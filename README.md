# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Report
* Basic Component
    * Pencil
        * 第1列第1行鉛筆圖案按鍵
        * 可藉由下方的Size的Range Slider調整筆刷大小
        * 可藉由下方的RGB三色的Range Slider調整筆刷顏色
    * Eraser
        * 第2列第1行橡皮擦圖案按鍵
        * 可藉由下方的Size的Range Slider調整橡皮擦大小
    * Text
        * 第4列第1行Text圖案按鍵
        * 在下方Text input欄位鍵入文字，隨後在畫布上點擊印出文字
        * 可藉由下方的Font Style的Select Option選擇字型 (typeface)
        * 可藉由下方的Font Size的Range Slider調整字體大小
        * 可藉由下方的RGB三色的Range Slider調整字體顏色
        * 可藉由下方的"fill"及"stroke"按鍵決定字體是否實心
    * Cursor Icon
        * 根據選取的工具，在Canvas畫布上呈現不同的游標
    * Refresh Button
        * 第4列第2行Reset圖案按鍵
        * 按下後直接清空畫布
        * 可被"Undo"復原清空的畫布
* Advance Tools
    * Circle
        * 第1列第2行圓圈圖案按鍵
        * 按下拉開放掉後產生圓形
        * 可藉由下方的Size的Range Slider調整邊長粗細
        * 可藉由下方的RGB三色的Range Slider調整圓圈顏色
        * 可藉由下方的"fill"及"stroke"按鍵決定"空心"或"填滿"
    * Rectangle
        * 第2列第2行矩形圖案按鍵
        * 按下拉開放掉後產生矩形
        * 可藉由下方的Size的Range Slider調整邊長粗細
        * 可藉由下方的RGB三色的Range Slider調整矩形顏色
        * 可藉由下方的"fill"及"stroke"按鍵決定"空心"或"填滿"
    * Triangle
        * 第3列第2行三角形圖案按鍵
        * 在畫布上任意點擊三次，將以此三點形成三角形
        * 可藉由下方的Size的Range Slider調整邊長粗細
        * 可藉由下方的RGB三色的Range Slider調整三角形顏色
        * 可藉由下方的"fill"及"stroke"按鍵決定"空心"或"填滿"
    * Undo/Redo
        * 分別為第5列第1行Undo圖案按鍵及第5列第2行Redo圖案按鍵
        * 可分別執行Undo及Redo
        * 對於Refresh過的畫布可被Undo復原
    * Download
        * 最下方Download圖案按鍵
        * 直接以jpeg格式下載當前畫布
        * 根據下載次數給予檔案名稱及編號： my_canvas_1, my_canvas_2, ......
* Other Useful Widgets
    * Line
        * 第3列第2行圓圈圖案按鍵
        * 按下拉開放掉後產生直線
        * 可藉由下方的Size的Range Slider調整直線粗細
        * 可藉由下方的RGB三色的Range Slider調整直線顏色
    * Fill/Stroke
        * 第6列實心矩形及空心矩形之按鍵
        * 可決定字體或圖形是否實心或空心
        * 預設為實心


